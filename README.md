# jpop-api-gateway

https://dzone.com/articles/how-to-build-an-api-gateway-with-netflix-zuul-spri  
https://howtodoinjava.com/spring-cloud/spring-cloud-api-gateway-zuul/  
https://medium.com/@nepalBrothers/api-gateway-using-spring-boot-zuul-part-1-6fb943c8efb8    
https://stackabuse.com/spring-cloud-routing-with-zuul-and-gateway/  
https://dzone.com/articles/service-discovery-with-eureka-and-zuul 
https://stackoverflow.com/questions/5009096/files-showing-as-modified-directly-after-a-git-clone

# AWS  
Create account on aws
log in to console
create users in IAM for each role

Deploy:
Go to Services -> Compute -> EC2  
Create Instance -> Launch Instance  
In case you are developer and learning -> check "Free Tire only" on left side  
Select any of the Free AMI available  
  1. Choose AMI  
  2. Choose Instance type  
  3. Configure Instance  
  4. Add storage  
  5. Add tags : A tag consists of a case-sensitive key-value pair. For example, you could define a tag with key = Name and value = Webserver.
                A copy of a tag can be applied to volumes, instances or both.
                Tags will be applied to all instances and volumes.  
  6. Configure security group: A security group is a set of firewall rules that control the traffic for your instance. On this page, you can add rules to allow specific traffic to reach your instance. For example, if you want to set up a web server and allow Internet traffic to reach your instance, add rules that allow unrestricted access to the HTTP and HTTPS ports. You can create a new security group or select from an existing one below  
  7. Review and launch  
  
You have to create new key pair:
"A key pair consists of a public key that AWS stores, and a private key file that you store. Together, they allow you to connect to your instance securely. For Windows AMIs, the private key file is required to obtain the password used to log into your instance. For Linux AMIs, the private key file allows you to securely SSH into your instance.  
Note: The selected key pair will be added to the set of keys authorized for this instance"  

How to connect to just created instance via putty:  
https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/putty.html  

Download Putty: https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html    
Download PuttyGen (Convert pem to ppk file):  
Download Winscp: https://winscp.net/eng/download.php  

Note: 
1. You must create a security group and not use the default security group as you will not be be to connect in that case.      
2. Every time you start and stop server 'Public DNS (IPv4)' and 'IPv4 Public IP' changes    
3. To start jar run: 'java -jar jarName.jar'   
4. Update java version: sudo yum install java-1.8.0, sudo yum remove java-1.7.0-openjdk    

 
# Docker  
## Resources   
Docker.com  -> Resources    
hub.docker.com -> Signup -> download "Docker Desktop"  

Commands:  
docker -v  
docker version  
docker info  
docker help  

docker container ls -all  

docker ps (Only running containers)  
docker ps -a (shows even the stopped containers)  

Ubuntu  
docker image pull ubuntu  
docker image ls  
docker container run ubuntu : To simply run  
docker container run -it ubuntu: to run and get inside the container  

Tomcat  
docker run tomcat:9  
docker run -p 9090:8080 tomcat:9 - run tomcat 9 version on 8080 port in container and map that port to 9090 of local machine  

Dockerfile - default file name for docker  
docker build -t jpop-docker-example . (. resembles current directory)  
docker build -t jpop-docker-example -f MyDockerfile . (-f commit in case file name is different from the default name)  

Share image:  
docker login  
docker build -t girirajvyas/jpop-docker-example . (repo name has to appended while building, by default latest tag)  
docker build -t girirajvyas/jpop-docker-example:v1 . (with tag name)  
docker tag girirajvyas/jpop-docker-example:v1 girirajvyas/jpop-docker-example:v2 (copies image with new tag)  
docker push girirajvyas/jpop-docker-example  
docker pull girirajvyas/jpop-docker-example  
docker image rm girirajvyas/jpop-docker-example (remove image)  

Dockerize spring boot  
docker pull mysql  
docker run -p 3306:3306 --name mysqldb -e MYSQL_ROOT_PASSWORD=root -e MSQL_DATABASE=users -d mysql:latest  
docker logs mysqldb (name that you used above)  
docker logs mysqldb -f (continous logs)  

Create Network  
docker network create --driver bridge micro-service-network  
--network micro-service-network (pass this parameter while starting containers to be on the same network)  
  
docker run -p 3306:3306 --name mysqldb -e MYSQL_ROOT_PASSWORD=root -e MYSQL_DATABASE=users -d mysql:latest --network micro-service-network  

